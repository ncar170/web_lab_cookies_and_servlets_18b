package ictgradschool.web.lab16.examples.exercise01;

public class UserDetails {

    private String fname;
    private String lname;
    private String country;
    private String city;

    public UserDetails (String fname, String lname, String country, String city){
        this.fname = fname;
        this.lname = lname;
        this.country = country;
        this.city = city;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public String getFname() {
        return fname;
    }

    public String getLname() {
        return lname;
    }

    @Override
    public String toString() {
        return "Welcome " + fname + " " + lname + " from " + city + ", " + country + ".";
    }
}

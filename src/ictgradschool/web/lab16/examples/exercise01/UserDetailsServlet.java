package ictgradschool.web.lab16.examples.exercise01;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;

public class UserDetailsServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        String fname = request.getParameter("fnameAttr");
        String lname = request.getParameter("lnameAttr");
        String country = request.getParameter("countryAttr");
        String city = request.getParameter("cityAttr");

        UserDetails userDetails = new UserDetails(fname, lname, country, city);

        PrintWriter out = response.getWriter();

        // Header stuff
        out.println("<!DOCTYPE html>");
        out.println("<html lang=\"en\">");
        out.println("<head>");
        out.println("<meta charset=\"UTF-8\">");
        out.println("<title>Web Lab 16 Examples - Sessions</title>");
        out.println("</head>");

        out.println("<body>");
        out.println("<a href=\"index.html\">HOME</a><br>");

        out.println("<h3>Data entered: </h3>");
        //TODO - add the firstName, lastName, city and country  that were entered into the form to the list below
        //TODO - add the parameters from the form to session attributes

        HttpSession session = request.getSession(true);

        session.setAttribute("user", userDetails);

        out.println("<ul>");
        out.println("<li>First Name: " + userDetails.getFname() + "</li>");
//        session.setAttribute("fname", fname);
        out.println("<li>Last Name: " + userDetails.getLname() + "</li>");
//        session.setAttribute("lname", lname);
        out.println("<li>Country: " + userDetails.getCountry() + "</li>");
//        session.setAttribute("country", country);
        out.println("<li>City: " + userDetails.getCity() +  "</li>");
//        session.setAttribute("city", city);
        out.println("</ul>");

        Cookie fNameCookie = new Cookie("fname", fname);
        response.addCookie(fNameCookie);

        Cookie lNameCookie = new Cookie("lname", lname);
        response.addCookie(lNameCookie);

        Cookie countryCookie = new Cookie("country", URLEncoder.encode(country, "UTF-8"));
//        Cookie countryCookie = new Cookie("country", country);
        response.addCookie(countryCookie);

        Cookie cityCookie = new Cookie("city", city);
        response.addCookie(cityCookie);

        out.println("<body>");

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

}

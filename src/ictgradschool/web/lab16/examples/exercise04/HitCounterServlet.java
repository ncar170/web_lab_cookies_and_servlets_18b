package ictgradschool.web.lab16.examples.exercise04;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class HitCounterServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Cookie[] cookies = req.getCookies(); //create an array of cookies by requesting all existing cookies
        Cookie hits = null; //create a Cookie object called hits, but don't assign it a new value
        int counter = 0;

        if ("true".equals(req.getParameter("removeCookie"))) { //if the parameter 'removeCookie' is checked, then delete cookie
            for (Cookie c: cookies ) {
                if (c.getName().equals("hits")){
                    c.setMaxAge(0); //this sets max age of the cookie to 0, which essentially deletes the cookie
                }
                resp.addCookie(c); //adds the cookie back to the response (which has already expired)
            }
        } else {
            if (cookies != null){ //check if cookies exist
                for (Cookie c: cookies){ //loop through all cookies (if they exist)
                    if (c.getName().equals("hits")){ //check if there is a cookie called hits
                        counter = Integer.parseInt(c.getValue()); //assign the value in hits to counter
                    }
                }
                counter++; //increment counter
                hits = new Cookie("hits", String .valueOf(counter)); //create a new hits cookie with new value of counter (this basically overrides the old cookie with new value)
                resp.addCookie(hits); //add cookies back to the response
            }

            //TODO - add code here to get the value stored in the 'hits' cookie then increase it by 1 and update the cookie

        }

        //TODO - use the response object's send redirect method to refresh the page
        resp.sendRedirect("hit-counter.html"); //this redirects the response back to the original html page

    }
}
